﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_7_Collections
{
    public class SalaryService<T> where T : class, IWorker, new() //Ограничения входного типа до типа с реализацией интерфейса IWorker
    {
        public T Worker { get; set; }

        public SalaryService(T worker)
        {
            Worker = worker;
        }

        public int SendMoney(params int[] koefs)
        {
            int sum = 0;
            foreach (var item in koefs)
            {
                sum += item;
            }
            return sum;
        }
    }
}
