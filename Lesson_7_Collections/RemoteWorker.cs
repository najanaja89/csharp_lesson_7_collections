﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_7_Collections
{
    public class RemoteWorker : IWorker
    {
        public int Salary { get; set; }
        public int WorkHours { get; set; }

        public void DoWork()
        {
            Console.WriteLine("Do anything");
        }
    }
}
