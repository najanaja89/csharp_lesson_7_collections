﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_7_Collections
{
    public class RegularWorker : IWorker
    {
        public int Salary { get; set; }
        public string Position { get; set; }
        public int WorkDaysCount { get; set; }

        public void DoWork()
        {
            Console.WriteLine("regular worker doing almost nothing");
        }
    }
}
