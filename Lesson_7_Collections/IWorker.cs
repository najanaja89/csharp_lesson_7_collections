﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_7_Collections
{
    public interface IWorker
    {
        int Salary { get; set; }
        void DoWork();

    }
}
