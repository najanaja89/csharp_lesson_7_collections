﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_7_Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Colletions
            List<string> collection = new List<string>();
            collection.Add("Stroke1");
            collection.Add("Stroke2");

            Dictionary<DayOfWeek, List<string>> weekMeal = new Dictionary<DayOfWeek, List<string>>();

            weekMeal.Add(DayOfWeek.Monday, new List<string>
            {
                "Coffee",
                "Apple",
                "Orange"
            }
            );

            HashSet<DayOfWeek> dayOfWeeks = new HashSet<DayOfWeek>();
            dayOfWeeks.Add(DayOfWeek.Monday);

            foreach (var item in weekMeal[DayOfWeek.Monday])
            {
                Console.WriteLine(item);
            }
            #endregion

            RemoteWorker remoteWorker = new RemoteWorker();
            RegularWorker regularWorker = new RegularWorker();
            SalaryService<RegularWorker> salaryService = new SalaryService<RegularWorker>(regularWorker);
            var worker = salaryService.Worker;
            Console.WriteLine((int)salaryService.SendMoney(worker.Salary, worker.WorkDaysCount));

            Console.ReadLine();
        }
    }
}
